from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    globaldenomination=math.Object(
        PARENT=networks.nets['globaldenomination'],
        SHARE_PERIOD=10, # seconds
        NEW_SHARE_PERIOD=10, # seconds
        CHAIN_LENGTH=24*60*60//10, # shares
        REAL_CHAIN_LENGTH=24*60*60//10, # shares
        TARGET_LOOKBEHIND=200, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=30, # blocks
        NEW_SPREAD=30, # blocks
        IDENTIFIER='2e828c9658a4f1d0'.decode('hex'),
        PREFIX='E31B56f5359e39ad'.decode('hex'),
        P2P_PORT=8348,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=8347,
        BOOTSTRAP_ADDRS='gdn.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-gdn',
        VERSION_CHECK=lambda v: True,
    ),

    nanolite=math.Object(
        PARENT=networks.nets['nanolite'],
        SHARE_PERIOD=10, # seconds
        NEW_SHARE_PERIOD=10, # seconds
        CHAIN_LENGTH=24*60*60//10, # shares
        REAL_CHAIN_LENGTH=24*60*60//10, # shares
        TARGET_LOOKBEHIND=200, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=30, # blocks
        NEW_SPREAD=30, # blocks
        IDENTIFIER='9ce289dea086bc75'.decode('hex'),
        PREFIX='c565c429e7bda4dc'.decode('hex'),
        P2P_PORT=23747,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=23746,
        BOOTSTRAP_ADDRS='nl.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-nl',
        VERSION_CHECK=lambda v: True,
    ),

    litecoinx=math.Object(
        PARENT=networks.nets['litecoinx'],
        SHARE_PERIOD=10, # seconds
        NEW_SHARE_PERIOD=10, # seconds
        CHAIN_LENGTH=24*60*60//10, # shares
        REAL_CHAIN_LENGTH=24*60*60//10, # shares
        TARGET_LOOKBEHIND=200, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=30, # blocks
        NEW_SPREAD=30, # blocks
        IDENTIFIER='3ea7d75f4e024292'.decode('hex'),
        PREFIX='8a4929b1a85930c8'.decode('hex'),
        P2P_PORT=11316,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=11315,
        BOOTSTRAP_ADDRS='ltcx.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-ltcx',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
